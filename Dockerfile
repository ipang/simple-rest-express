FROM node:12-alpine
MAINTAINER ipang

RUN mkdir -p /express
RUN mkdir -p /express/node_modules
WORKDIR /express
ENV PATH /express/node_modules/.bin:$PATH

COPY package.json /express/package.json
RUN npm install
COPY . /express

EXPOSE 3000 
CMD ["node", "server.js"]
